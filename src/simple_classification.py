#!/usr/bin/python
#encoding: utf-8

import pandas as pd

hist = pd.read_csv("../data/raw/history.csv", sep=";")
fifa = pd.read_csv("../data/raw/fifa.csv", sep=";")

def clean_text(t):
    t = t.strip()
    t = t.upper()
    return t

def clean_team(df, col):
    #df = df[pd.notnull(df[col])]
    df = df.dropna(subset=[col])
    df[col] = df[col].map(clean_text)
    return df

hist = hist[["home_team", "visiting_tram", "score_team1_rt", "score_team2_rt"]]
hist.columns = ["t1", "t2", "s1", "s2"]
hist = clean_team(hist, "t1")
hist = clean_team(hist, "t2")
print hist.head()



fifa = fifa[["Team", "Total Points"]]
fifa = clean_team(fifa, "Team")

fifa.columns = ["t1", "p1"]
print fifa

hist = hist.join(fifa, on="t1", lsuffix="hist", rsuffix="fifa")
fifa.columns = ["t2", "p2"]
hist = hist.join(fifa, on="t2", lsuffix="hist", rsuffix="fifa")

print hist
print hist.count()