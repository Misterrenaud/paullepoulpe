#!/usr/bin/python
# encoding: utf8
import time

from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pprint import pprint
import slugify
from pyspark.mllib.clustering import KMeans, KMeansModel
from tqdm import tqdm

from numpy import array
import math
import joblib
import datetime



def clean_name(t):
    try:
        t = slugify.slugify(t, separator=u"", stopwords=["city"])
    except Exception:
        print t
    return t

def ts_from_txt(text):
    text = text[:10]
    if int(text[5:7]) > 12:
        text = text[0:4] + "-" + text[8:10] + "-" + text[5:7]
        print "changed to ", text

    try:
        return time.mktime(datetime.datetime.strptime(text, "%Y-%m-%d").timetuple())
    except ValueError:
        print "error on ", text
        return None


if __name__ == '__main__':


    spark = SparkSession.builder.getOrCreate()
    sc = spark.sparkContext

    fifa = (
        spark.read.csv("../data/raw/fifa.csv", sep=";", header=True, inferSchema=True)
        .select("Team", "Total Points")
        .rdd.map(lambda x: x.asDict())
        .map(lambda x: (
            clean_name(x["Team"]),
            x["Total Points"],
        ))
    )

    print fifa.first()
    print fifa.count()



    hist = (
        spark.read.csv("../data/raw/history.csv", sep=";", header=True, inferSchema=True)
        .withColumn("date", F.concat_ws("-", "year", F.lpad("month", 2, '0'), F.lpad("day", 2, "0")))
        .select("home_team", "visiting_tram", "score_team1_rt", "score_team2_rt", "date")
    ).union(
        spark.read.csv("../data/raw/results.csv", sep=",", header=True, inferSchema=True)
        .select(
            F.col("home_team").alias("home_team"),
            F.col("away_team").alias("visiting_tram"),
            F.col("home_score").alias("score_team1_rt"),
            F.col("away_score").alias("score_team2_rt"),
            F.col("date").alias("date"),
        )
    )

    print hist.first()
    print hist.count()

    rdd = hist.rdd.map(lambda x: x.asDict())

    rdd_clean = (
        rdd.map(lambda x: {
            "t1": clean_name(x["home_team"]),
            "t2": clean_name(x["visiting_tram"]),
            "s1": x["score_team1_rt"],
            "s2": x["score_team2_rt"],
            "time": ts_from_txt(x["date"])
        })
    )
    rdd_double = rdd_clean.union(
        rdd_clean.map(lambda x: {
                "t1": x["t2"],
                "t2": x["t1"],
                "s1": x["s2"],
                "s2": x["s1"],
                "time": x["time"],
            })
    )

    print rdd_double.take(1)
    print rdd_double.count()







    j1 = rdd_double.keyBy(lambda x: x["t1"]).leftOuterJoin(fifa).values().map(lambda x: dict(x[0], p1=x[1]))
    j2 = j1.keyBy(lambda x: x["t2"]).leftOuterJoin(fifa).values().map(lambda x: dict(x[0], p2=x[1]))

    print j2.take(1)
    print j2.count()


    filtered = j2.filter(lambda x: None not in x.values())
    print filtered.take(1)
    print filtered.count()

    filtered = filtered.filter(lambda x: x["time"] > time.mktime(datetime.datetime(2015, 1, 1).timetuple()) )
    print filtered.take(1)
    print filtered.count()
    data_set_size = filtered.count()

    filtered = filtered.repartition(32).cache()

    import matplotlib.pyplot as plt
    """
    diff = filtered.map(lambda x: x["p1"] - x["p2"]).collect()
    summ = filtered.map(lambda x: x["p1"] + x["p2"]).collect()
    catt_dif = filtered.map(lambda x: x["s1"] - x["s2"]).collect()
    catt_sum = filtered.map(lambda x: x["s1"] + x["s2"]).collect()
    
    
    
    
    
    plt.scatter(diff, summ,s=100, c=catt_dif)
    
    plt.show()
    
    """


    parsedData = filtered.map(lambda x: (x, array([
        x["p1"] - x["p2"],
        x["p1"] + x["p2"],
        x["time"]/(3600*7),
    ])))


    x = []
    y = []

    k = data_set_size / 10

    # Build the model (cluster the data)
    clusters = KMeans.train(parsedData.values(), k)

    # Evaluate clustering by computing Within Set Sum of Squared Errors
    def error(point):
        center = clusters.centers[clusters.predict(point)]
        return math.sqrt(sum([x**2 for x in (point - center)]))


    clusters.save(sc, "../data/KMeansModel")


    # clusters = KMeansModel.load(sc, "../data/KMeansModel")


    res = (
        parsedData
        .mapValues(lambda x: clusters.predict(x))
        .map(lambda (x,v): ((x["s1"] - x["s2"], x["s1"] + x["s2"]), v))
        .map(lambda x: (x[1], (x[0], 1)))
        .reduceByKey(lambda a,b: ((a[0][0] + b[0][0], a[0][1] + b[0][1]), a[1]+b[1]))
        .mapValues(lambda x: (1.0 * x[0][0] / x[1], 1.0 * x[0][1] / x[1]))
        .collectAsMap()
    )

    joblib.dump(res, "../data/scoresFromModel")


