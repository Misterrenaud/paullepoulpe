#!/usr/bin/python
# encoding: utf8
import csv

from pyspark.sql import SparkSession
import datetime
from main import clean_name
from numpy import array
import joblib
import time
from pyspark.mllib.clustering import KMeansModel

spark = SparkSession.builder.getOrCreate()
sc = spark.sparkContext



fifa = (
    spark.read.csv("../data/raw/fifa.csv", sep=";", header=True, inferSchema=True)
    .select("Team", "Total Points")
    .rdd.map(lambda x: x.asDict())
    .map(lambda x: (
        clean_name(x["Team"]),
        x["Total Points"],
    ))
)
fifa_local = fifa.collectAsMap()


clusters = KMeansModel.load(sc, "../data/KMeansModel")
res = joblib.load("../data/scoresFromModel")



def predict(team_a, team_b, date):
    p1 = fifa_local.get(clean_name(team_a))
    p2 = fifa_local.get(clean_name(team_b))

    v = array([
        p1 - p2,
        p1 + p2,
        time.mktime(date.timetuple())/(3600*7),
    ])

    c = clusters.predict(v)

    r = res.get(c)

    diff = r[0]
    summ = r[1]
    print "sum", summ, "diff", diff, "cluster", c
    return int(round((diff + summ) / 2)), int(round((summ - diff) / 2))



output = []
with open('../data/raw/Prediction.csv', 'rb') as f:
    reader = csv.reader(f, delimiter=";")

    for row in reader:
        if output == []:
            output.append(row)
        else:
            pred = predict(row[3], row[4], datetime.datetime(2018, int(row[2]), int(row[1])))
            print row[3], row[4], pred

            o = row
            o[5] = pred[0]
            o[6] = pred[1]
            output.append(o)


with open('../data/rloiseleux.csv', 'wb') as f:
    # Overwrite the old file with the modified rows
    writer = csv.writer(f, delimiter=";")
    writer.writerows(output)






