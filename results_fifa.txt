Pour vous aider dans vos réflexions, voici le résultat théorique en ne tenant compte que du classement FIFA:

Groupe A:
1A = Uruguay - 2A = Egypt
Groupe B:
1B = Portugal - 2B = Spain
Groupe C:
1C = France - 2C = Peru
Groupe D:
1D = Argentina - 2D = Croatia
Groupe E:
1E = Brazil - 2E = Switzerland
Groupe F:
1F = Germany - 2F = Mexico
Groupe G:
1G = Belgium - 2G = England
Groupe H:
1H = Poland - 2H = Colombia

8ème:
Match 49:Uruguay - Spain ->  Spain
Match 50:France - Croatia ->  France
Match 53:Brazil - Mexico ->  Brazil
Match 54:Belgium - Colombia ->  Belgium
Match 51:Portugal - Egypt ->  Portugal
Match 52:Argentina - Peru ->  Argentine
Match 55:Germany - Switzerland ->  Germany
Match 56:Poland - England ->  Poland

Quarts:
Match 57:Spain - France ->  Spain
Match 58:Brazil - Belgium ->  Brazil
Match 59:Portugal - Argentina ->  Portugal
Match 60:Germany - Poland ->  Germany

Demi-finales:
Match 61:Spain - Brazil ->  Brazil
Match 62:Portugal - Germany ->  Germany

3ème place:
Spain - Portugal ->  Portugal

Finale:
Brazil - Germany ->  Germany